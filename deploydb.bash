
???
#mlfactory-feeder-tiger
#go build -o bin/countpogrebrec gitlab.com/remotejob/mlfactory-feederv5/cmd/countPogrebrec
#scp bin/countpogrebrec root@157.245.2.244:/root/rpc/feeder/
go build -o bin/rpcfeeder gitlab.com/remotejob/mlfactory-feederv5/cmd/rpcfeedersml && \
ssh root@157.245.2.244 systemctl stop nlpfeeder.service && \
#scp feeder.conf.toml.prod root@157.245.2.244:/root/rpc/feeder/feeder.conf.toml && \
scp bin/rpcfeeder root@157.245.2.244:/root/rpc/feeder/ && \
ssh root@157.245.2.244 systemctl start nlpfeeder.service


#on tensorflow2
#scp singo.db  root@159.203.67.26:/root/rpc/feeder/
#scp feeder.conf.toml.prod root@159.203.67.26:/root/rpc/feeder/feeder.conf.toml
#scp -r templates  root@159.203.67.26:/root/rpc/feeder/
ssh root@159.203.67.26 systemctl stop nlpfeeder.service && \
#scp feeder.conf.toml.prod root@159.203.67.26:/root/rpc/feeder/feeder.conf.toml && \
scp bin/rpcfeeder root@159.203.67.26:/root/rpc/feeder/ && \
ssh root@159.203.67.26 systemctl start nlpfeeder.service


#on mltiger-feeder00
#scp singo.db  root@157.245.219.161:/root/rpc/feeder/
#scp feeder.conf.toml.prod root@157.245.219.161:/root/rpc/feeder/feeder.conf.toml
#scp -r templates root@157.245.219.161:/root/rpc/feeder/
#scp bin/rpcfeeder root@157.245.219.161:/root/rpc/feeder/ 
#systemctl stop nlpfeeder.service  #make snap!
#ssh root@157.245.219.161 systemctl stop nlpfeeder.service && \
#scp bin/rpcfeeder root@157.245.219.161:/root/rpc/feeder/ && \
#ssh root@157.245.219.161 systemctl start nlpfeeder.service


# 11.12.2019 recque
# go build -o bin/rpcfeeder gitlab.com/remotejob/mlfactory-feederv5/cmd/rpcfeeder && \
# ssh root@64.225.0.202 systemctl stop nlpfeeder.service && \
# scp bin/rpcfeeder root@64.225.0.202:/root/rpc/feeder/ && \
# ssh root@64.225.0.202 systemctl start nlpfeeder.service

#httpsproxy
# scp -r templates root@64.225.9.26:/root/rpc/feeder/
# scp feeder.conf.toml.prod root@64.225.9.26:/root/rpc/feeder/feeder.conf.toml
# scp singo.db root@64.225.9.26:/root/rpc/feeder/
# scp bin/rpcfeeder root@64.225.9.26:/root/rpc/feeder/
ssh root@64.225.9.26 systemctl stop nlpfeeder.service && \
#scp feeder.conf.toml.prod root@64.225.9.26:/root/rpc/feeder/feeder.conf.toml && \
scp bin/rpcfeeder root@64.225.9.26:/root/rpc/feeder/ && \
ssh root@64.225.9.26 systemctl start nlpfeeder.service




#collector
# scp -r templates root@138.197.103.130:/root/rpc/feeder/
# scp feeder.conf.toml.prod root@138.197.103.130:/root/rpc/feeder/feeder.conf.toml
# scp singo.db root@138.197.103.130:/root/rpc/feeder/
# scp bin/rpcfeeder root@138.197.103.130:/root/rpc/feeder
ssh root@138.197.103.130 systemctl stop nlpfeeder.service && \
#scp feeder.conf.toml.prod root@138.197.103.130:/root/rpc/feeder/feeder.conf.toml && \
scp bin/rpcfeeder root@138.197.103.130:/root/rpc/feeder/ && \
ssh root@138.197.103.130 systemctl start nlpfeeder.service

#vi  /usr/lib/systemd/system/nlpfeeder.service
#systemctl enable nlpfeeder.service
#systemctl start nlpfeeder.service
#systemctl restart nlpfeeder.service
#journalctl -f -u nlpfeeder.service

#common-tiger00
#vi  /lib/systemd/system/nlpfeeder.service
# scp -r templates root@209.97.151.172:/root/rpc/feeder/
# scp feeder.conf.toml.prod root@209.97.151.172:/root/rpc/feeder/feeder.conf.toml
# scp singo.db root@209.97.151.172:/root/rpc/feeder/
# scp bin/rpcfeeder root@209.97.151.172:/root/rpc/feeder
ssh root@209.97.151.172 systemctl stop nlpfeeder.service && \
#scp feeder.conf.toml.prod root@209.97.151.172:/root/rpc/feeder/feeder.conf.toml && \
scp bin/rpcfeeder root@209.97.151.172:/root/rpc/feeder/ && \
ssh root@209.97.151.172 systemctl start nlpfeeder.service


#common-tiger02
#vi  /lib/systemd/system/nlpfeeder.service
# scp -r templates root@45.55.40.206:/root/rpc/feeder/
# scp feeder.conf.toml.prod root@45.55.40.206:/root/rpc/feeder/feeder.conf.toml
# scp singo.db root@45.55.40.206:/root/rpc/feeder/
# scp bin/rpcfeeder root@45.55.40.206:/root/rpc/feeder
ssh root@45.55.40.206 systemctl stop nlpfeeder.service && \
#scp feeder.conf.toml.prod root@45.55.40.206:/root/rpc/feeder/feeder.conf.toml && \
scp bin/rpcfeeder root@45.55.40.206:/root/rpc/feeder/ && \
ssh root@45.55.40.206 systemctl start nlpfeeder.service



#mltigerfeeder
#vi  /lib/systemd/system/nlpfeeder.service
# scp -r templates root@167.172.230.166:/root/rpcv5/feeder/
# scp feeder.conf.toml.prod root@167.172.230.166:/root/rpcv5/feeder/feeder.conf.toml
# scp sqldata/singo.db root@167.172.230.166:/root/rpcv5/feeder/sqldata
# scp sqldata/mlphrasesfi.db root@167.172.230.166:/root/rpcv5/feeder/sqldata
scp bin/rpcfeeder root@167.172.230.166:/root/rpcv5/feeder
ssh root@167.172.230.166 systemctl stop nlpfeeder.service && \
#scp feeder.conf.toml.prod root@:167.172.230.166/root/rpc/feeder/feeder.conf.toml && \

ssh root@167.172.230.166 systemctl start nlpfeeder.service

systemctl start nlpfeederv5.service
journalctl -f -u nlpfeederv5.service




scp root@167.172.230.166:mlphrasesfinew.db sqldata/mlphrasesfi.db
sqlite3 sqldata/mlphrasesfi.db 
delete FROM mlphrasesbest where created_at <DATE('now','-15 day');
delete FROM mlphrases where created_at <DATE('now','-15 day');
vacuum;

tar zcfv mlphrasesfi.db.tar.gz sqldata/mlphrasesfi.db 

#chatproxynlp
#vi  /lib/systemd/system/nlpfeeder.service
# scp -r templates root@165.227.71.179:/root/rpcv5/feeder/
# scp feeder.conf.toml.prod root@165.227.71.179:/root/rpcv5/feeder/feeder.conf.toml
# scp sqldata/singo.db root@165.227.71.179:/root/rpcv5/feeder/sqldata
# scp sqldata/mlphrasesfi.db root@165.227.71.179:/root/rpcv5/feeder/sqldata
scp mlphrasesfi.db.tar.gz  root@165.227.71.179:/root/rpcv5/feeder/ && \
# scp bin/rpcfeeder root@165.227.71.179:/root/rpcv5/feeder
ssh root@165.227.71.179 systemctl stop nlpfeeder.service && \
ssh root@165.227.71.179 'cd rpcv5/feeder && tar zxfv mlphrasesfi.db.tar.gz && rm mlphrasesfi.db.tar.gz'
#scp feeder.conf.toml.prod root@:167.172.230.166/root/rpc/feeder/feeder.conf.toml && \
ssh root@165.227.71.179 systemctl start nlpfeeder.service

# systemctl start nlpfeederv5.service
# journalctl -f -u nlpfeederv5.service



#collector
#scp root@167.172.230.166:mlphrasesfi.db sqldata/
#tar zcfv mlphrasesfi.db.tar.gz sqldata/mlphrasesfi.db 
#vi  /lib/systemd/system/nlpfeeder.service
# scp -r templates root@138.197.103.130:/root/rpcv5/feeder/
# scp feeder.conf.toml.prod root@138.197.103.130:/root/rpcv5/feeder/feeder.conf.toml
# scp sqldata/singo.db root@138.197.103.130:/root/rpcv5/feeder/sqldata
scp mlphrasesfi.db.tar.gz  root@138.197.103.130:/root/rpcv5/feeder/ && \
# scp bin/rpcfeeder root@138.197.103.130:/root/rpcv5/feeder
ssh root@138.197.103.130 systemctl stop nlpfeeder.service && \
ssh root@138.197.103.130 'cd rpcv5/feeder && tar zxfv mlphrasesfi.db.tar.gz && rm mlphrasesfi.db.tar.gz'
ssh root@138.197.103.130 systemctl start nlpfeeder.service

systemctl start nlpfeeder.service
journalctl -f -u nlpfeeder.service
