package configdb

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	// "net/rpc"

	badger "github.com/dgraph-io/badger"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/templatefunc"
)

type Constants struct {
	Badger struct {
		MaxRecords int
		Sleep int
	}
	Rest struct {
		Url string
	}

	Activedomains struct {
		Url string
	}

	Checknewdomains struct {
		Hours int
	}

	Rpcserver struct {
		Url      string
		MaxPages int
		MinPages int
	}
	Db struct {
		Url    string
		Stmt0  string
		Limit0 int
		Stmt1  string
		Limit1 int
	}

	Variants struct {
		Quant    int
		Varhtml0 string
		Js0      string
		Varhtml1 string
		Js1      string
		Varhtml2 string
		Js2      string
		Varhtml3 string
		Js3      string
		Varhtml4 string
		Js4      string
		Varhtml5 string
		Js5      string
		Varhtml6 string
		Js6      string
	}
}
type Config struct {
	Constants
	Database *sql.DB
	BadgerDB *badger.DB
	// RpcClient *rpc.Client
	Variant0 *template.Template
	Variant1 *template.Template
	Variant2 *template.Template
	Variant3 *template.Template
	Variant4 *template.Template
	Variant5 *template.Template
	Variant6 *template.Template
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}

	funcMap := template.FuncMap{

		"slug":     templatefunc.Slug,
		"rawjs":    templatefunc.Rawjs,
		"linktext": templatefunc.LinkText,
	}
	var0html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml0))
	config.Variant0 = var0html
	var1html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml1))
	config.Variant1 = var1html
	var2html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml2))
	config.Variant2 = var2html
	var3html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml3))
	config.Variant3 = var3html
	var4html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml4))
	config.Variant4 = var4html
	var5html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml5))
	config.Variant5 = var5html
	var6html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml6))
	config.Variant6 = var6html

	litedb, err := sql.Open("sqlite3", config.Constants.Db.Url)
	if err != nil {
		return &config, err

	}

	config.Database = litedb

	dbb, err := badger.Open(badger.DefaultOptions("badger"))
	if err != nil {
		log.Fatal(err)
	}
	config.BadgerDB = dbb
	return &config, err
}
func initViper() (Constants, error) {
	viper.SetConfigName("dbfeeder.conf") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")           // Search the root directory for the configuration file
	err := viper.ReadInConfig()        // Find and read the config file
	if err != nil {                    // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	// viper.SetDefault("PORT", "6000")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
