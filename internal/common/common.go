package common

import (

	"math/rand"
	"time"

	"github.com/oklog/ulid/v2"
)

func Random(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(max-min) + min
}

func Buulid() ([]byte, error) {

	var res []byte
	t := time.Now()
	entropy := ulid.Monotonic(rand.New(rand.NewSource(t.UnixNano())), 0)

	uuid, err := ulid.New(ulid.Timestamp(t), entropy)
	if err != nil {
		return nil,err 
	}
	
	uuidbin, err := uuid.MarshalBinary()
	if err != nil {
		return nil, err
	}


	res = uuidbin

	return res, nil


}

func RandInt(n int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(n)
}

func Even(number int) bool {
    return number%2 == 0
}
func Odd(number int) bool {
    // Odd should return not even.
    // ... We cannot check for 1 remainder.
    // ... That fails for negative numbers.
    return !Even(number)
}
