package config

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"

	"github.com/xujiajun/nutsdb"
	
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/templatefunc"
)

type Constants struct {
	BPHRASES int64
	Kvdb     struct {
		MaxRecords int64
		MinRecords int64
	}
	Nutsdb struct {
		Url string
	}
	Rest struct {
		Url string
	}
	Restrescue struct {
		Url string
	}
	Activedomains struct {
		Url string
	}

	Checknewdomains struct {
		Hours int
	}

	Rpcserver struct {
		Url      string
		MaxPages int64
		MinPages int64
	}
	Db struct {
		Url    string
		Stmt1  string
		Limit1 int
	}
	MlDb struct {
		Url             string
		Stmtphrasesbest string
		Stmtphrases     string
	}
	Variants struct {
		Quant    int
		Varhtml0 string
		Js0      string
		Varhtml1 string
		Js1      string
		Varhtml2 string
		Js2      string
		Varhtml3 string
		Js3      string
		Varhtml4 string
		Js4      string
		Varhtml5 string
		Js5      string
		Varhtml6 string
		Js6      string
	}
}
type Config struct {
	Constants
	Database   *sql.DB
	Mldatabase *sql.DB
	// BadgerDB *badger.DB
	NutsDB *nutsdb.DB
	// RpcClient *rpc.Client
	Variant0 *template.Template
	Variant1 *template.Template
	Variant2 *template.Template
	Variant3 *template.Template
	Variant4 *template.Template
	Variant5 *template.Template
	Variant6 *template.Template
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}

	funcMap := template.FuncMap{

		"slug":     templatefunc.Slug,
		"rawjs":    templatefunc.Rawjs,
		"linktext": templatefunc.LinkText,
		// "saveHTML": templatefunc.SaveHTML,
	}
	var0html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml0))
	config.Variant0 = var0html
	var1html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml1))
	config.Variant1 = var1html
	var2html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml2))
	config.Variant2 = var2html
	var3html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml3))
	config.Variant3 = var3html
	var4html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml4))
	config.Variant4 = var4html
	var5html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml5))
	config.Variant5 = var5html
	var6html := template.Must(template.New("index.html").Funcs(funcMap).ParseGlob(config.Constants.Variants.Varhtml6))
	config.Variant6 = var6html

	litedb, err := sql.Open("sqlite3", config.Constants.Db.Url)
	if err != nil {
		return &config, err

	}

	config.Database = litedb

	mldb, err := sql.Open("sqlite3", config.Constants.MlDb.Url)
	if err != nil {
		return &config, err

	}

	config.Mldatabase = mldb

	opt := nutsdb.DefaultOptions
	opt.Dir = config.Constants.Nutsdb.Url
	db, err := nutsdb.Open(opt)
	if err != nil {
		log.Fatal(err)
	}

	config.NutsDB = db
	

	return &config, err
}
func initViper() (Constants, error) {
	viper.SetConfigName("feeder.conf") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")           // Search the root directory for the configuration file
	err := viper.ReadInConfig()        // Find and read the config file
	if err != nil {                    // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
