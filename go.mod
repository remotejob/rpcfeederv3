module gitlab.com/remotejob/mlfactory-feederv5

go 1.13

require (
	github.com/akrylysov/pogreb v0.8.3
	github.com/dgraph-io/badger v1.6.0
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gosimple/slug v1.9.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/mattn/go-sqlite3 v2.0.2+incompatible
	github.com/oklog/ulid/v2 v2.0.2
	github.com/spf13/viper v1.6.1
	github.com/xujiajun/nutsdb v0.5.0
	gitlab.com/remotejob/mlengine v1.0.0
	gitlab.com/remotejob/mlfactory-feederv3 v0.0.1
	gitlab.com/remotejob/mlfactory-feederv4 v0.0.0-20200106020653-2325a52723f6
	gitlab.com/remotejob/mltigerfeeder v0.0.0-20191218042729-79268b7a31cd
	golang.org/x/sys v0.0.0-20200106162015-b016eb3dc98e // indirect
)
