package main

import (
	"log"
	"net/rpc"
	"strings"
	"time"

	"gitlab.com/remotejob/mlfactory-feederv5/internal/common"
	"gitlab.com/remotejob/mlfactory-feederv5/internal/config"
	"gitlab.com/remotejob/mlfactory-feederv5/internal/domains"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/activedomains"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/capFirstChar"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/checklastchar"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/createbytepage"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/dbhandler"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/finalcleanup"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/randbestphrases"
	"gitlab.com/remotejob/mltigerfeeder/pkg/cleanphrase"

	// "gitlab.com/remotejob/mlfactory-feederv5/pkg/createbytepage"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/getdata"
	// "gitlab.com/remotejob/mlfactory-feederv5/pkg/getnewdata"
	// "gitlab.com/remotejob/mlfactory-feederv5/pkg/pogrebhandler"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/rescueactdom"

	_ "github.com/mattn/go-sqlite3"
)

var (
	conf     *config.Config
	err      error
	count    int64
	countint int
	activedom  []string
	marktime   time.Time
	orgphrases []string
	client     *rpc.Client
	reply      domains.Item
	chartoscheck map[string]struct{}
)

func init() {

	conf, err = config.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}

	orgphrases, err = getdata.GetOnlyOrg(conf.Database, conf.Constants.Db.Stmt1, conf.Constants.Db.Limit1)
	if err != nil {

		log.Fatalln(err)
	}

	activedom, err = activedomains.Get(conf.Activedomains.Url)
	if err != nil {

		activedom = rescueactdom.Rescue(err)

	}

	log.Println("activedom num at init()", len(activedom))
	client, err = rpc.DialHTTP("tcp", conf.Constants.Rpcserver.Url)
	if err != nil {
		log.Fatal("Connection error: ", err)
	}

	chartoscheck := make(map[string]struct{}, 0)

	chartoscheck["."] = struct{}{}
	chartoscheck["?"] = struct{}{}
	chartoscheck["!"] = struct{}{}

}
func main() {

	// stop := make(chan os.Signal)
	// signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT)

	// go func() {
	// 	defer signal.Stop(stop)
	// 	sig := <-stop
	// 	log.Println(sig.String())
	// 	conf.PogrebDB.Close()

	// }()

	// bestphrases, err := dbhandler.GetBestPhrases(conf)
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	for {

		client.Call("API.Count", "", &countint)

		log.Println("count start loop", countint)
		count = int64(countint)

		if count < conf.Constants.Rpcserver.MaxPages {

			bestphrases, err := dbhandler.GetBestPhrases(conf)
			if err != nil {
				log.Fatalln(err)
			}

			phrases, err := dbhandler.GetPhrases(conf)
			if err != nil {

				log.Println(err)
			}

			var outphrases []domains.Phrase
			var outres []string

			for i, phrase := range phrases {

				clines := cleanphrase.Clean(phrase.Txt, chartoscheck)
				if len(clines) > 1 {
					bestphrase := randbestphrases.GetBest(conf, bestphrases, len(clines))

					cssnum := common.RandInt(len(bestphrase))

					var chbestphrase string
					for i, bphrase := range bestphrase {
						if i == cssnum {
							chbestphrase = checklastchar.CheckChange(bphrase, chartoscheck, ". ", true)
						} else {
							chbestphrase = checklastchar.CheckChange(bphrase, chartoscheck, ". ", false)
						}
						outres = append(outres, chbestphrase)
						outres = append(outres, clines[i])
					}

					if common.Odd(i) && i != 0 {
						ntxt := strings.Join(outres, "")
						phrase.Txt = finalcleanup.Finalclean(ntxt)

						outphrases = append(outphrases, phrase)
						outres = outres[:0]

					} else {

						outres = append(outres, "<h1>"+capFirstChar.Cap(phrase.Title)+".</h1>")

					}

				}

			}

			items := createbytepage.CreateMany(outphrases, conf, orgphrases, activedom)

			for _, item := range items {

				client.Call("API.AddItem", item, &reply)

			}
		}
		for count > conf.Constants.Rpcserver.MaxPages {

			time.Sleep(30 * time.Second)

			client.Call("API.Count", "", &countint)

			count = int64(countint)

			log.Println("count in sleep", count)

			orgphrases, err = getdata.GetOnlyOrg(conf.Database, conf.Constants.Db.Stmt1, conf.Constants.Db.Limit1)
			if err != nil {

				log.Fatalln(err)
			}

			activedom, err = activedomains.Get(conf.Activedomains.Url)
			if err != nil {

				activedom = rescueactdom.Rescue(err)

			}

			log.Println("activedom num in sleep try reconnect RPC", len(activedom))

			err = client.Close()
			if err != nil {
				log.Fatalln(err)

			} else {

				client, err = rpc.DialHTTP("tcp", conf.Constants.Rpcserver.Url)
				if err != nil {
					log.Fatal("Connection error: ", err)
				}
			}

		}

	}

	// var count int

	// client.Call("API.Count", "", &count)

	// log.Println("count",count)

	// for {

	// count, brecords, err = check(conf)
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	// log.Println("count", count, "brecords", brecords)

	// if count == 0 {

	// 	keys, vals, err := pogrebhandler.GetAll(conf, 100)
	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}

	// 	for _, v := range vals {

	// 		var item domains.Item
	// 		item.Page = v

	// 		client.Call("API.AddItem", item, &reply)

	// 	}

	// 	err = pogrebhandler.DeletePages(conf, keys)
	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}

	// 	log.Fatalln("Count 0 !!!try insert 100 rec and RESTART")

	// }

	// if (count > conf.Rpcserver.MaxPages) && (brecords > conf.Badger.MaxRecords) {
	// 	log.Println("RPC and DB full wait 60 sec reconnect")
	// 	time.Sleep(60 * time.Second)

	// 	err = client.Close()
	// 	if err != nil {
	// 		log.Fatalln(err)

	// 	} else {

	// 		client, err = rpc.DialHTTP("tcp", conf.Constants.Rpcserver.Url)
	// 		if err != nil {
	// 			log.Fatal("Connection error: ", err)
	// 		}
	// 	}

	// } else {

	// 	if count < conf.Rpcserver.MaxPages/4 {

	// 		remdatas, err := getnewdata.GetRemoteDatas(conf.Rest.Url)
	// 		if err != nil {

	// 			// log.Fatalln(err)
	// 			log.Println(err.Error(), "try resque")

	// 			remdatasrescue, err := getnewdata.GetRemoteDatas(conf.Restrescue.Url)
	// 			if err != nil {

	// 				log.Println(err.Error(), "resque NOT OK", conf.Restrescue.Url)

	// 			} else {

	// 				items := createbytepage.CreateMany(remdatasrescue, conf, orgphrases, activedom)

	// 				for _, item := range items {

	// 					client.Call("API.AddItem", item, &reply)

	// 				}

	// 			}

	// 		} else {

	// 			items := createbytepage.CreateMany(remdatas, conf, orgphrases, activedom)

	// 			for _, item := range items {

	// 				client.Call("API.AddItem", item, &reply)

	// 			}
	// 		}

	// 	}

	// 	if (count < conf.Rpcserver.MaxPages) && (brecords > conf.Badger.MaxRecords/10) {

	// 		getnumrec := conf.Rpcserver.MaxPages - count + 5000
	// 		getnumrecuint32 := uint32(getnumrec)

	// 		log.Println("getnumrec getnumrecuint32 brecords", getnumrec, getnumrecuint32, brecords)

	// 		if getnumrecuint32 > brecords {

	// 			getnumrec = int(brecords)

	// 		}

	// 		log.Println("Get from db getnumrec", getnumrec)

	// 		keys, vals, err := pogrebhandler.GetAll(conf, getnumrec)
	// 		if err != nil {
	// 			log.Fatal(err)
	// 		}

	// 		for _, v := range vals {

	// 			var item domains.Item
	// 			item.Page = v

	// 			client.Call("API.AddItem", item, &reply)

	// 		}

	// 		err = pogrebhandler.DeletePages(conf, keys)
	// 		if err != nil {
	// 			log.Fatal(err)
	// 		}

	// 	} else if brecords < conf.Badger.MaxRecords {

	// 		log.Println("Create brecords brecords")

	// 		remdatas, err := getnewdata.GetRemoteDatas(conf.Rest.Url)
	// 		if err != nil {

	// 			// log.Fatalln(err)

	// 			// log.Fatalln(err)
	// 			log.Println(err.Error(), "try resque")

	// 			remdatasrescue, err := getnewdata.GetRemoteDatas(conf.Restrescue.Url)
	// 			if err != nil {

	// 				log.Println(err.Error(), "resque NOT OK", conf.Restrescue.Url)

	// 			} else {

	// 				items := createbytepage.CreateMany(remdatasrescue, conf, orgphrases, activedom)

	// 				if len(items) > 0 {

	// 					pogrebhandler.InsertAllQue(conf, items)

	// 				}

	// 			}

	// 		} else {

	// 			items := createbytepage.CreateMany(remdatas, conf, orgphrases, activedom)

	// 			pogrebhandler.InsertAllQue(conf, items)
	// 		}
	// 	}

	// }

	// }
}

// func check(conf *config.Config) (int, uint32, error) {

// 	var count int

// 	brecords, err := pogrebhandler.CountPages(conf)
// 	if err != nil {
// 		return 0, 0, err
// 	}
// 	client.Call("API.Count", "", &count)

// 	return count, brecords, nil

// }
