package dbhandler

import (
	"database/sql"
	// "log"
	"strconv"
	"gitlab.com/remotejob/mlfactory-feederv5/internal/config"
	"gitlab.com/remotejob/mlfactory-feederv5/internal/domains"
)

func GetBestPhrases(conf *config.Config) ([]string, error) {


	var phrs []string
	rows, err := conf.Mldatabase.Query(conf.MlDb.Stmtphrasesbest)
	if err != nil {

		return nil, err
	}

	for rows.Next() {
		var phrase string
		rows.Scan(&phrase)
		phrs = append(phrs, phrase)

	}

	return phrs, nil

}

func GetPhrases(conf *config.Config) ([]domains.Phrase, error) {

	// sqlStatement := "SELECT rowid,prompt,orgtxt FROM mlphrases ORDER BY random() limit " + rec

	var retres []domains.Phrase
	var prompt string
	var phrase string
	var rowid int64

	rows, err := conf.Mldatabase.Query(conf.MlDb.Stmtphrases)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		err := rows.Scan(&rowid, &prompt, &phrase)
		if err != nil {
			return nil, err
		}
		phr := domains.Phrase{rowid, prompt, phrase}
		retres = append(retres, phr)

	}

	return retres, nil
}
func Get(db *sql.DB, stmt string, limit int) ([]string, error) {

	stmtfull := stmt + strconv.Itoa(limit)

	phrases := make([]string, limit)

	rows, err := db.Query(stmtfull)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var phrase string
	var count int

	for rows.Next() {
		rows.Scan(&phrase)
		phrases[count] = phrase
		count++
	}
	return phrases, nil
}
