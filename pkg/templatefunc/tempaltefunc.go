package templatefunc

import (
	"html/template"
	"strings"

	"github.com/gosimple/slug"
)

func Slug(s string) string {

	fields := strings.Fields(s)
	if strings.HasPrefix(fields[0],"http://") {

		rslice := fields[1:]

		slack := slug.MakeLang(strings.Join(rslice," "), "fi")

		return fields[0] + "/" +slack
	}

	link := slug.MakeLang(s, "fi")

	return "/" +link

}

func Rawjs(s string) template.JS {

	return template.JS(s)
}

func LinkText(s string) string {

	fields := strings.Fields(s)

	if strings.HasPrefix(fields[0],"http://") {

		rslice := fields[1:]

		return strings.Join(rslice," ")
	}

	return strings.Join(fields," ")
}

// func SaveHTML(s string) template.HTML {

// 	return template.HTML(s)
// }