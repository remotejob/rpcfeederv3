package randbestphrases

import (
	"math/rand"
	"time"

	"gitlab.com/remotejob/mlfactory-feederv5/internal/config"
)

func GetBest(conf *config.Config,bestphrases []string, needednum int) []string {

	var resout []string

	rand.Seed(time.Now().UnixNano())

	for i := 1; i < needednum; i++ {
		rand.Seed(time.Now().UnixNano())
		resout = append(resout, bestphrases[rand.Intn(int(conf.Constants.BPHRASES))])

	}

	return resout

}