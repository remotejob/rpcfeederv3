package createitem

import (
	"bytes"
	"html/template"
)

func Create(tpl *template.Template, name string, data interface{}) ([]byte, error) {	

	buf := new(bytes.Buffer)
	if err := tpl.ExecuteTemplate(buf, name, data); err != nil {

		return nil, err
	}

	return buf.Bytes(), nil

}
